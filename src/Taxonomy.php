<?php

namespace Kindling\PostTypes;

use Kindling\PostTypes\MergesArguments;

class Taxonomy
{
    use MergesArguments;

    /**
     * Taxonomy arguments
     *
     * @var array
     */
    protected $arguments;

    /**
     * Taxonomy slug
     *
     * @var string
     */
    protected $taxonomy;

    /**
     * Post type
     *
     * @var string
     */
    protected $type;

    /**
     * Singular title.
     *
     * @var string
     */
    protected $singularTitle;

    /**
     * Plural title.
     *
     * @var string
     */
    protected $pluralTitle;

    /**
     * Constructor
     *
     * @param string $taxonomy
     * @param string $type
     * @param array $arguments
     */
    public function __construct($taxonomy, $type, $arguments)
    {
        $this->arguments = $arguments;
        $this->taxonomy = $taxonomy;
        $this->type = $type;
        $title = ucwords(str_replace(['_', '-'], ' ', $type));
        $this->singularTitle = "{$title} Category";
        $this->pluralTitle = "{$title} Categories";
    }

    /**
     * Initalize the taxonomy.
     *
     * @return void
     */
    public function init()
    {
        if (is_null($this->taxonomy) or taxonomy_exists($this->taxonomy)) {
            return;
        }

        register_taxonomy($this->taxonomy, [ $this->type ], $this->baseArguments());
    }

    /**
     * Gets the taxonomy arguments when registering a taxonomy using register_taxonomy()
     *
     * @see http://codex.wordpress.org/Function_Reference/register_taxonomy
     * @return array
     */
    public function baseArguments()
    {
        return $this->taxonomyArgumentMerge([
            'hierarchical' => true,
            'labels' => [
                'name' => _x("{$this->pluralTitle}", 'taxonomy general name', 'taxonomy general name'),
                'singular_name' => _x("{$this->singularTitle}", 'taxonomy singular name', 'taxonomy singular name'),
                'search_items' => __("Search {$this->pluralTitle}"),
                'all_items' => __("All {$this->pluralTitle}"),
                'parent_item' => __("Parent {$this->singularTitle}"),
                'parent_item_colon' => __("Parent {$this->singularTitle}:"),
                'edit_item' => __("Edit {$this->singularTitle}"),
                'update_item' => __("Update {$this->singularTitle}"),
                'add_new_item' => __("Add New {$this->singularTitle}"),
                'new_item_name' => __("New {$this->singularTitle} Name"),
                'menu_name' => __("{$this->pluralTitle}"),
                'view_item' => __("View {$this->singularTitle}"),
                'popular_items' => __("Popular {$this->pluralTitle}"),
                'separate_items_with_commas' => __("Separate {$this->pluralTitle} with commas"),
                'add_or_remove_items' => __("Add or remove {$this->pluralTitle}"),
                'choose_from_most_used' => __("Choose from the most used {$this->pluralTitle}"),
                'not_found' => __("No {$this->pluralTitle} found."),
                'no_terms' => "No {$this->pluralTitle}",
                'items_list_navigation' => "{$this->pluralTitle} list navigation",
                'items_list' => "{$this->pluralTitle} list",
                'most_used' => "Most Used",
                'back_to_items' => "&larr; Back to {$this->pluralTitle}",
                'name_admin_bar' => "Other {$this->singularTitle}",
                'archives' => "All Other {$this->pluralTitle}",
            ],
            'public' => true,
            'show_in_nav_menus' => true,
            'show_ui' => true,
            'show_tagcloud' => true,
            'show_admin_column' => true,
            'query_var' => $this->taxonomy,
            'rewrite' => [
                'slug' => "{$this->type}-categories",
                'with_front' => false,
                'hierarchical' => true,
            ],
        ], $this->arguments);
    }

    /**
     * Get the value of singularTitle
     *
     * @return string
     */
    public function getSingularTitle()
    {
        return $this->singularTitle;
    }

    /**
     * Set the value of singularTitle
     *
     * @param string $singularTitle
     * @return self
     */
    public function setSingularTitle($singularTitle)
    {
        $this->singularTitle = $singularTitle;

        return $this;
    }

    /**
     * Get the value of pluralTitle
     *
     * @return string
     */
    public function getPluralTitle()
    {
        return $this->pluralTitle;
    }

    /**
     * Set the value of pluralTitle
     *
     * @param string $pluralTitle
     * @return self
     */
    public function setPluralTitle($pluralTitle)
    {
        $this->pluralTitle = $pluralTitle;

        return $this;
    }
}
