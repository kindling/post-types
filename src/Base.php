<?php
/**
 * Kindling Type Base Class.
 *
 * @package Kindling_Post_Types
 * @author  Matchbox Design Group <info@matchboxdesigngroup.com>
 */

namespace Kindling\PostTypes;

use Kindling\PostTypes\Taxonomy;
use Kindling\PostTypes\HasImageColumn;
use Kindling\PostTypes\MergesArguments;
use Illuminate\Support\Str;

/**
 * This is a base for custom post type classes so they can all take advantage of the same logic and defaults.
 *
 * @example wp-content/themes/kindling/src/Type/Stub.php
 */

abstract class Base
{
    use HasImageColumn, MergesArguments;

    /**
     * The slug of the post types landing page if not post type archive.
     *
     * @var string
     */
    protected $landingPageSlug;

    /**
     * The slug of the post types landing page template if not post type archive.
     *
     * @var string
     */
    protected $landingPageTemplate;

    /**
     * Slug for post type.
     *
     * @var string
     */
    protected $type;

    /**
     * Title of post type.
     *
     * @var string
     */
    protected $pluralTitle;

    /**
     * Singular title.
     *
     * @var string
     */
    protected $singularTitle;

    /**
     * The taxonomy "name" used in register_taxonomy().
     *
     * @var array
     */
    protected $taxonomy;

    /**
     * The taxonomy class.
     *
     * @var \Kindling\PostTypes\Taxonomy
     */
    protected $taxonomyClass;

    /**
     * Class constructor, takes care of all the setup needed.
     *
     * @param string $type The post type id.
     * @param string $pluralTitle The post type plural title.
     * @param string $singularTitle The post type singular title.
     */
    public function __construct($type, $pluralTitle = null, $singularTitle = null)
    {
        $this->type = $type;
        $singularTitle = $singularTitle ?: str_replace(['-', '_'], ' ', $type);
        $pluralTitle = $pluralTitle ?: Str::plural($singularTitle);
        $this->singularTitle = ucwords($singularTitle);
        $this->pluralTitle = ucwords($pluralTitle);
        $this->taxonomy = "{$this->type}-categories";
        $this->landingPageSlug = Str::slug($pluralTitle);
        $this->landingPageTemplate = "template-{$this->type}";
    }

    /**
     * Sets up the post type.
     *
     * @return Kindling\PostTypes\Base
     */
    public function init()
    {
        // Create post type and taxonomy.
        $this->register();

        // Featured image column action.
        $this->addImageColumnAction($this->type);

        return $this;
    }

    /**
     * Registers the post type and a custom taxonomy for the post type..
     */
    protected function register()
    {
        if (post_type_exists($this->type)) {
            return;
        }

        // Register post type
        register_post_type($this->type, $this->baseArguments());

        // Register taxonomy
        $this->getTaxonomyClass()->init();
    }

    /**
     * Gets the taxonomy class
     *
     * @return \Kindling\PostTypes\Taxonomy
     */
    public function getTaxonomyClass()
    {
        if (isset($this->taxonomyClass)) {
            return $this->taxonomyClass;
        }

        return $this->taxonomyClass = new Taxonomy(
            $this->taxonomy,
            $this->type,
            $this->taxonomyArguments()
        );
    }

    /**
     * Gets the arguments used for registering the post type with register()
     *
     * @see http://codex.wordpress.org/Function_Reference/register
     *
     * @return array
     */
    public function baseArguments()
    {
        // Lowercase Labels
        $lcPluralTitle = strtolower($this->pluralTitle);
        $lcSingularTitle = strtolower($this->singularTitle);

        return $this->typeArgumentMerge([
            'labels' => [
                'name' => __($this->pluralTitle),
                'singular_name' => __($this->singularTitle),
                'add_new' => __("Add New {$this->singularTitle}"),
                'add_new_item' => __("Add News {$this->singularTitle}"),
                'edit_item' => __("Edit {$this->singularTitle}"),
                'new_item' => __("New {$this->singularTitle}"),
                'all_items' => __("All {$this->pluralTitle}"),
                'view_item' => __("View {$this->singularTitle}"),
                'search_items' => __("Search {$this->pluralTitle}"),
                'not_found' => __("No {$lcPluralTitle} found"),
                'not_found_in_trash' => __("No {$lcPluralTitle} found in Trash"),
                'parent_item_colon' => __(''),
                'menu_name' => __($this->pluralTitle),
                'featured_image' => __("{$this->singularTitle} Image"),
                'set_featured_image' => __("Set {$lcSingularTitle} image"),
                'remove_featured_image' => __("Remove {$lcSingularTitle} image"),
                'use_featured_image' => __("Use as {$lcSingularTitle} image"),
                'view_items' => "View {$this->pluralTitle}",
                'archives' => "All {$this->pluralTitle}",
                'attributes' => "{$this->singularTitle} Attributes",
                'insert_into_item' => "Insert into {$lcSingularTitle}",
                'uploaded_to_this_item' => "Uploaded to this {$lcSingularTitle}",
                'filter_items_list' => "Filter {$lcPluralTitle} list",
                'items_list_navigation' => "{$this->pluralTitle} list navigation",
                'items_list' => "{$this->pluralTitle} list",
                'name_admin_bar' => $this->singularTitle,
            ],
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'query_var' => true,
            'rewrite' => [
                'slug' => "{$this->type}s",
                'with_front' => false
            ],
            'capability_type' => 'post',
            'has_archive' => false,
            'hierarchical' => true,
            'menu_position' => 5,
            'can_export' => true,
            'supports' => [
                'title',
                'editor',
                'post-thumbnails',
                'custom-fields',
                'page-attributes',
                'author',
                'thumbnail',
                'excerpt',
                'trackbacks',
                'comments',
                'revisions',
                'post-formats',
            ],
            'menu_icon' => 'dashicons-edit',
        ], $this->arguments());
    }

    /**
     * The post type arguments.
     * Overwrite this method in a sub-class to overwrite default post type arguments.
     *
     * @return array
     */
    public function arguments()
    {
        return [];
    }

    /**
     * The taxonomy arguments.
     * Overwrite this method in a sub-class to overwrite default taxonomy arguments.
     *
     * @return array
     */
    public function taxonomyArguments()
    {
        return [];
    }

    /**
     * Retrieves the current post types posts.
     *
     * @see http://codex.wordpress.org/Class_Reference/WP_Query
     *
     * @param array   $arguments Optional. Any arguments accepted by the WP_Query class.
     * @param boolean $getQuery  Optional. If true it will return the WP_Query object instead of posts.
     *
     * @return array Retrieved post objects/Query object.
     */
    public function getPosts($arguments = [], $getQuery = false)
    {
        $query = new WP_Query(array_merge([
            'post_type'=> $this->type,
            'posts_per_page' => 500,
            'post_status'=> 'publish',
            'order' => 'DESC',
            'orderby' => 'date',
        ], $arguments));

        return $getQuery ? $query : $query->get_posts();
    }

    /**
     * Get the slug of the post types landing page if not post type archive.
     *
     * @return  string
     */
    public function getLandingPageSlug()
    {
        return $this->landingPageSlug;
    }

    /**
     * Set the slug of the post types landing page if not post type archive.
     *
     * @param  string  $landingPageSlug  The slug of the post types landing page if not post type archive.
     *
     * @return  self
     */
    public function setLandingPageSlug(string $landingPageSlug)
    {
        $this->landingPageSlug = $landingPageSlug;

        return $this;
    }

    /**
     * Get the slug of the post types landing page template if not post type archive.
     *
     * @return  string
     */
    public function getLandingPageTemplate()
    {
        return $this->landingPageTemplate;
    }

    /**
     * Set the slug of the post types landing page template if not post type archive.
     *
     * @param  string  $landingPageTemplate  The slug of the post types landing page template if not post type archive.
     *
     * @return  self
     */
    public function setLandingPageTemplate(string $landingPageTemplate)
    {
        $this->landingPageTemplate = $landingPageTemplate;

        return $this;
    }

    /**
     * Get slug for post type.
     *
     * @return  string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get title of post type.
     *
     * @return  string
     */
    public function getPluralTitle()
    {
        return $this->pluralTitle;
    }

    /**
     * Get singular title.
     *
     * @return  string
     */
    public function getSingularTitle()
    {
        return $this->singularTitle;
    }

    /**
     * Get the taxonomy "name" used in register_taxonomy().
     *
     * @return  array
     */
    public function getTaxonomy()
    {
        return $this->taxonomy;
    }

    /**
     * Set the taxonomy "name" used in register_taxonomy().
     *
     * @param  string|null  $taxonomy  The taxonomy "name" used in register_taxonomy().
     *
     * @return  self
     */
    public function setTaxonomy($taxonomy)
    {
        $this->taxonomy = $taxonomy;

        return $this;
    }
}
