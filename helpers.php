<?php
/**
 * Kindling Post Type Functions.
 *
 * @package Kindling_Post_Types
 * @author  Matchbox Design Group <info@matchboxdesigngroup.com>
 */

use Illuminate\Contracts\Container\Container as ContainerContract;

/**
 * Gets the fully name spaced post type classes.
 *
 * @return Illuminate\Support\Collection
 */
function kindling_post_types_get_types()
{
    return collect(apply_filters('kindling_post_types_type_classes', []))->unique();
}

/**
 * Add multiple post type classes.
 *
 * @param array $classes
 * @return void
 */
function kindling_post_types_add_types($classes)
{
    collect($classes)->each(function ($class) {
        add_filter('kindling_post_types_type_classes', function ($classes) use ($class) {
            $classes[] = $class;

            return $classes;
        });
    });
}

/**
 * Remove multiple post type classes.
 *
 * @param array $types
 * @return void
 */
function kindling_post_types_remove_types(array $types)
{
    collect($types)->each(function ($type) {
        // We need to get the built in post types since we can
        // not technically unregister them just the ui
        $builtins = collect(get_post_types())->map(function ($type) {
            return get_post_type_object($type);
        })->filter(function ($type) {
            return $type->_builtin;
        })->pluck('name');

        // Unregister the post type if it is not built in.
        if (!$builtins->contains($type)) {
            unregister_post_type($type);
        }

        // Remove menu item no matter what.
        add_action('admin_menu', function () use ($type) {
            remove_menu_page(
                $type === 'post' ? 'edit.php' : "edit.php?post_type={$type}"
            );
        }, 99);
    });
}

/**
 * Registers the post types
 *
 * @return void
 */
function kindling_post_types_register_types()
{
    kindling_post_types_get_types()->each(function ($class) {
        if (!class_exists($class)) {
            return false;
        }

        // Add the post type to the container.
        $abstract = explode('\\', $class);
        $abstract = 'type.' . end($abstract);
        kindling()->singleton($abstract, function (ContainerContract $app) use ($class) {
            $class = new $class;

            $class->init();

            return $class;
        });

        // Resolve the post type out of the container to initialize it.
        kindling()->make($abstract);
    });
}
