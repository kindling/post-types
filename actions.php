<?php
/**
 * Kindling Post Type Actions.
 *
 * @package Kindling_Post_Types
 * @author  Matchbox Design Group <info@matchboxdesigngroup.com>
 */

if (!function_exists('add_action')) {
    return;
}

add_action('kindling_ready', function () {
    /**
     * Register post types.
     * Add fully name spaced classes using the kindling_post_types_type_classes filter.
     * All post classes must extend Kindling\PostTypes\Base see src/Type/Stub.php for an example.
     */
    add_action('init', 'kindling_post_types_register_types');
});
